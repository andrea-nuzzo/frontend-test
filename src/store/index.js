import axios from 'axios';
import { createStore } from 'vuex';


const options = {
    headers: {
      Authorization: "Bearer keyEC9F0usYdVoWzg",
    },
  };

export default createStore({
  state: {
    profileList: [],
  },
  mutations: {
    setProfileList(state, profileList) {
      state.profileList = profileList;
    },
  },
  actions: {
    async fetchProfileList({ commit }) {
      const response = await axios.get("https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/People", options);
      commit('setProfileList', response.data.records);
    },
  },
});