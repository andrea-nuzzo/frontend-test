import { createApp } from "vue";
import App from "./App.vue";
import routes from "@/routes";
import store from '@/store'
import './css/style.css'

const profileApp = createApp(App);
profileApp.use(routes);
profileApp.use(store);

profileApp.mount('#app');
