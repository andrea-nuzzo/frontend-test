import Home from "../pages/Home";
import ProfileShow from "../pages/ProfileShow";
import NotFound from "../pages/NotFound";
import store from "@/store";

import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/profile/:id",
    name: "ProfileShow",
    component: ProfileShow,
    props: true,

    beforeEnter(to, from, next) {
      const profileList = store.state.profileList;

      //check if profile exists
      const profileExists = profileList.find(
        (profile) => profile.id === to.params.id
      );
      //if exists continue
      if (profileExists) {
        return next();
      } else {
        next({
          name: "NotFound",
          //preserve existing params query and hash
          params: { pathMatch: to.path.substring(1).split("/") },
          query: to.query,
          hash: to.hash,
        });
      }
    },
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    component: NotFound,
  },
];

export default createRouter({
  history: createWebHistory(),
  routes,
});
