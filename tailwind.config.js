module.exports = {
  content: ["./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      width: {
        "360": "360px",
        "190": "190px",
        "304": "304px"
      },
      height: {
        "244": "244px",
        "260": "260px",
        "462": "462px",
        "390": "390px"
      },
      colors: {
        "role-color": "#828282",
        "follow-button": "#8DCC7B"
      },
      screens: {
        desktop: "1440px",
      },
      fontSize: {
        header1: [
          "34px",
          {
            lineHeight: "39.88px",
            letterSpacing: "-2%",
          },
        ],
        header2: [
          "20px",
          {
            lineHeight: "23.09px",
            letterSpacing: "-2%",
          },
        ],
        bio: [
          "16px",
          {
            lineHeight: "18.77px",
            letterSpacing: "-2%",
          },
        ],
        role: [
          "13px",
          {
            lineHeight: "15.25px",
            letterSpacing: "-2%",
          },
        ],
        button: [
          "12px",
          {
            lineHeight: "14.08px",
            letterSpacing: "-2%",
          },
        ],
        
      },
    },
  },
  plugins: [],
};
